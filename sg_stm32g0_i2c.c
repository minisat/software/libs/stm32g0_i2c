#include "sg_stm32g0_i2c.h"

void SG_I2Cx_Init(I2C_TypeDef *I2Cx) {
	if (I2Cx == I2C1) {
		RCC->APBENR1 |= RCC_APBENR1_I2C1EN; 
		RCC->IOPENR |= RCC_IOPENR_GPIOBEN; 

		GPIOB->MODER &= ~(GPIO_MODER_MODE6_Msk | GPIO_MODER_MODE7_Msk);
		GPIOB->MODER |= (2 << GPIO_MODER_MODE6_Pos) | (2 << GPIO_MODER_MODE7_Pos);

		GPIOB->OTYPER |= GPIO_OTYPER_OT6 | GPIO_OTYPER_OT7;
		GPIOB->OSPEEDR |= (3 << GPIO_OSPEEDR_OSPEED6_Pos) | (3 << GPIO_OSPEEDR_OSPEED7_Pos);
		GPIOB->AFR[0] |= (6 << GPIO_AFRL_AFSEL6_Pos) | (6 << GPIO_AFRL_AFSEL7_Pos);

		RCC->CCIPR &= ~RCC_CCIPR_I2C1SEL_Msk;
		RCC->CCIPR |= (1 << RCC_CCIPR_I2C1SEL_Pos);
	} else {
		RCC->APBENR1 |= RCC_APBENR1_I2C2EN; 
		RCC->IOPENR |= RCC_IOPENR_GPIOAEN;  

		GPIOA->MODER &= ~(GPIO_MODER_MODE11_Msk | GPIO_MODER_MODE12_Msk); 
		GPIOA->MODER |= (2 << GPIO_MODER_MODE11_Pos) | (2 << GPIO_MODER_MODE12_Pos);

		GPIOA->OTYPER |= GPIO_OTYPER_OT11 | GPIO_OTYPER_OT12;
		GPIOA->OSPEEDR |= (3 << GPIO_OSPEEDR_OSPEED11_Pos) | (3 << GPIO_OSPEEDR_OSPEED12_Pos); 
		GPIOA->AFR[1] |= (6 << GPIO_AFRH_AFSEL11_Pos) | (6 << GPIO_AFRH_AFSEL12_Pos);

		RCC->CCIPR &= ~RCC_CCIPR_I2S1SEL_Msk;
		RCC->CCIPR |= (1 << RCC_CCIPR_I2S1SEL_Pos);
	}

	I2Cx->CR1 &= ~I2C_CR1_PE; 

	I2Cx->TIMINGR = (0x1 << 28) | // PRESC = 1
			(0x9 << 0)  | // SCLL = 9
			(0x3 << 8)  | // SCLH = 3
			(0x2 << 16) | // SDADEL = 2
			(0x3 << 20);  // SCLDEL = 3

	I2Cx->CR1 |= I2C_CR1_PE;
}

inline void SG_I2C1_Init() {
	SG_I2Cx_Init(I2C1);
}

inline void SG_I2C2_Init() {
	SG_I2Cx_Init(I2C2);
}

// TODO: создать функцию Scan

uint8_t SG_I2C_Write_Bytes(I2C_TypeDef *I2Cx, uint8_t slave_addr, uint8_t *data, uint16_t len) {
	uint32_t timeout;

	timeout = 0;
	while (I2Cx->ISR & I2C_ISR_BUSY) {
		if (++timeout >= SG_I2C_TIMEOUT) return 1;
	}

	I2Cx->CR2 = (slave_addr) | (len << I2C_CR2_NBYTES_Pos) | I2C_CR2_START;

	timeout = 0;
	while (!(I2Cx->ISR & I2C_ISR_TXIS)) {
		if (I2Cx->ISR & I2C_ISR_NACKF) return 1;
		if (++timeout >= SG_I2C_TIMEOUT) return 1;
	}

	for (uint16_t i = 0; i < len; i++) {
		timeout = 0;
		while (!(I2Cx->ISR & I2C_ISR_TXIS)) {
			if (I2Cx->ISR & I2C_ISR_NACKF) return 1;
			if (++timeout >= SG_I2C_TIMEOUT) return 1;
		}
		I2Cx->TXDR = data[i];
	}

	timeout = 0;
	while (!(I2Cx->ISR & I2C_ISR_TC)) {
		if (++timeout >= SG_I2C_TIMEOUT) return 1;
	}

	I2Cx->CR2 |= I2C_CR2_STOP;
	return 0;
}

uint8_t SG_I2C_Read_Bytes(I2C_TypeDef *I2Cx, uint8_t slave_addr, uint8_t *data, uint16_t size) {
    uint32_t timeout;

    timeout = 0;
    while (I2Cx->ISR & I2C_ISR_BUSY) {
        if (++timeout >= SG_I2C_TIMEOUT) return 1;
    }

    I2Cx->CR2 = (slave_addr) | (size << I2C_CR2_NBYTES_Pos) |
                I2C_CR2_START | I2C_CR2_RD_WRN;

    for (uint16_t i = 0; i < size; i++) {
        timeout = 0;
        while (!(I2Cx->ISR & I2C_ISR_RXNE)) {
            if (++timeout >= SG_I2C_TIMEOUT) return 1;
        }

        data[i] = I2Cx->RXDR;
    }

    timeout = 0;
    while (!(I2Cx->ISR & I2C_ISR_TC)) {
        if (++timeout >= SG_I2C_TIMEOUT) return 1;
    }

    I2Cx->CR2 |= I2C_CR2_STOP;
    return 0;
}

inline uint8_t SG_I2C_Write_Byte(I2C_TypeDef *I2Cx, uint8_t slave_addr, uint8_t data) {
	return SG_I2C_Write_Bytes(I2C2, slave_addr, &data, 1);
}

inline uint8_t SG_I2C_Write_Register(I2C_TypeDef *I2Cx, uint8_t slave_addr, uint8_t reg_addr, uint8_t data) {
	uint8_t tmp[]={reg_addr, data};
	return SG_I2C_Write_Bytes(I2Cx, slave_addr, tmp, 2);
}

inline uint8_t SG_I2C_Read_Register(I2C_TypeDef *I2Cx, uint8_t slave_addr, uint8_t reg_addr) {
	SG_I2C_Write_Bytes(I2Cx, slave_addr, &reg_addr, 1);
	SG_I2C_Read_Bytes(I2Cx, slave_addr, &reg_addr, 1);
	return reg_addr;
}

inline uint8_t SG_I2C_Read_Register_Multiple(I2C_TypeDef *I2Cx, uint8_t slave_addr, uint8_t reg_addr, uint8_t* data_out, uint8_t len) {
	SG_I2C_Write_Bytes(I2Cx, slave_addr, &reg_addr, 1);
	return SG_I2C_Read_Bytes(I2Cx, slave_addr, data_out, len);
}
