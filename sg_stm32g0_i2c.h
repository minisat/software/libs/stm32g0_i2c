#ifndef INC_I2C_H_
#define INC_I2C_H_

#include "stm32g0xx_hal.h"

#define SG_I2C_TIMEOUT		10000	// ticks

#ifdef __cplusplus
extern "C" {
#endif

void SG_I2C1_Init();
void SG_I2C2_Init();

/**
 * @brief Write multiple bytes
 * 
 * @param I2Cx I2C1 or I2C2
 * @param slave_addr 7-bit address, shifted << 1
 * @param data byte array to write
 * @param len 
 * @return uint8_t 0 if success, else 1
 */
uint8_t SG_I2C_Write_Bytes(I2C_TypeDef *I2Cx, uint8_t slave_addr, uint8_t *data, uint16_t len);

/**
 * @brief Read multiple bytes
 * 
 * @param I2Cx I2C1 or I2C2
 * @param slave_addr 7-bit address, shifted << 1
 * @param data byte array, which data will be written
 * @param size 
 * @return uint8_t 0 if success, else 1
 */
uint8_t SG_I2C_Read_Bytes(I2C_TypeDef *I2Cx, uint8_t slave_addr, uint8_t *data, uint16_t size);

/**
 * @brief Write one byte (may be obsolete)
 * 
 * @param I2Cx 
 * @param slave_addr 
 * @param data 
 * @return uint8_t 
 */
uint8_t SG_I2C_Write_Byte(I2C_TypeDef *I2Cx, uint8_t slave_addr, uint8_t data);

/**
 * @brief Write data to register
 *
 * @param I2Cx I2C1 or I2C2
 * @param slave_addr 7-bit address, shifted << 1
 * @param reg_addr Registry address
 * @param data to write
 * @return uint8_t 0 if success, else 1
 */
uint8_t SG_I2C_Write_Register(I2C_TypeDef *I2Cx, uint8_t slave_addr, uint8_t reg_addr, uint8_t data);

/**
 * @brief Read data from register
 * 
 * @param I2Cx I2C1 or I2C2
 * @param slave_addr 7-bit address, shifted << 1
 * @param reg_addr Registry address
 * @return uint8_t Data from register
 */
uint8_t SG_I2C_Read_Register(I2C_TypeDef *I2Cx, uint8_t slave_addr, uint8_t reg_addr);

/**
 * @brief Read multiple data bytes
 * 
 * @param I2Cx I2C1 or I2C2
 * @param slave_addr 7-bit address, shifted << 1
 * @param reg_addr Registry address
 * @param data_out byte array, which data will be written
 * @param len 
 * @return uint8_t 0 if success, else 1
 */
uint8_t SG_I2C_Read_Register_Multiple(I2C_TypeDef *I2Cx, uint8_t slave_addr, uint8_t reg_addr, uint8_t* data_out, uint8_t len);

#ifdef __cplusplus
}
#endif

#endif /* INC_I2C_H_ */
